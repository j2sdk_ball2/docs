# [rename current local branch]
git branch -m <new-name>

# [rename another local branch]
git branch -m <old-name> <new-name>

# [rename remote branch]
git push origin :<old-branch-name>
git push --set-upstream origin <new-branch-name>