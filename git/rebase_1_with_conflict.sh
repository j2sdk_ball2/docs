# When you have resolved this problem
git rebase --continue

# If you prefer to skip this patch
git rebase --skip

# To check out the original branch and stop rebasing
git rebase --abort