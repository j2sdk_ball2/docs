# [start ssh agent]
eval `ssh-agent -s`

# [git account location]
vi ~/.gitconfig

# [list, remove, add]
ssh-add -l
ssh-add -D
ssh-add -K ~/.ssh/<private-key>