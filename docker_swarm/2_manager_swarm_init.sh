docker-machine ssh manager1
docker swarm init --advertise-addr 192.168.99.103:2377
# or
# docker swarm init
exit

# or
# docker-machine ssh manager1 "docker swarm init --advertise-addr 192.168.99.103:2377"



# output
# Swarm initialized: current node (ylb1cya4rr9bv1sjgo7ntqsj2) is now a manager.

# To add a worker to this swarm, run the following command:

#     docker swarm join --token SWMTKN-1-1om0oc4f3tlw8jy9q14gmnhneqqkghzr13awyy0k8ukqtyylyl-3nh9hey9ag731htjmfhiv5s7h 192.168.99.103:2377

# To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.