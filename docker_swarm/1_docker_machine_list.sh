docker-machine ls

# NAME       ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
# default    -        virtualbox   Stopped                                       Unknown       
# manager1   -        virtualbox   Running   tcp://192.168.99.103:2376           v17.06.0-ce   
# worker1    -        virtualbox   Running   tcp://192.168.99.101:2376           v17.06.0-ce   
# worker2    -        virtualbox   Running   tcp://192.168.99.102:2376           v17.06.0-ce   