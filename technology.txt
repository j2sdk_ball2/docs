[Technology]

Departments
    DevOps 
    Infrastructure 
    API
    Web 
    Mobile 
    Test
    Database

Marven

Socket.io
Flux
Redux
Next.js
StrongLoop/Loopback
Hapi.js
Express
Koa
PM2
Boilerplate
ES5
ES6
ES7
1/2-Way Data Binding
Kubernate
Replication
Master/Slave
PG Pool
MS SQL Server
PostgreSQL
Redis
MongoDB
Hadoop
Scalar
Cotlin
Code Coverage
Webpack
Browserify
SystemJS
Margento
PM2
Staging Server
Circle CI (AWS)
Eslint (code check)
Rabbit MQ (message to client)
Elastic Search (db)
MSMQ
Load Balance/Cluster/Master Slave
***Blockchain***
Docker Compose
Docker Swarm
Server Authen/Kerboros
Webstorm
Yaml/Yml (yarn)
Clujure
Data Science
Machine Learning
---
Vagrant
Ansible
Symentic
Landing Page
---

Security
    RSA (public/private key)
    GPG (GnuPG)
    Hmac (checksum)
    XML RPC
    SSL/TLS

Web/Native
    Apollo
    RxJS

Serverless
    Firebase - เน้น app
    GraphQL - relational
    Ramda - 

[Feature]

Pagination by scrollbar
Search without click button
Sent email
Push noti
Chat box
