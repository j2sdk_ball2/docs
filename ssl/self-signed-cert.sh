# rsa vs dsa

# private key
# public key
# certificate: x509 certificate
# certificate authority:

# ssh-keygen: ใช้สำหรับ generate secure shell key (private, public) เท่านั้น
# openssl:
#     to create self-signed certificate:

# with passphase
# openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem

# without passphase
openssl req -x509 -nodes -new -newkey rsa:2048 -keyout nattawut.key -out nattawut.cert

# extension:
#     .pem: 
#         - based64 encoded 
#         - private + public key
#     .key:
#         - private key
#     .pub:
#         - public key
#     .p12: 
#         - windows format
#         - PKCS#7 or PKCS#12 
#     .cer:
#         - windows format
#     .der:
#         - windows format