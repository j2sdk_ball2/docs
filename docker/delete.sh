# delete exit containers
docker rm `docker ps -aq`

# delete unused images
docker rmi `docker images -aq`