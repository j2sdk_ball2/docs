docker ps
docker ps -a															see all processes

docker images
------------------------------------------
docker pull redis:latest
------------------------------------------
$ docker run -it paynow:latest											run default command in Dockerfile
/bin/sh: 1: [node,server.js]: not found
-----------------------------------------------------------------
$ docker run -it paynow:latest /bin/bash								run command in bash
$ docker run -itd redis:latest
$ docker run -itd redis:latest --name nauseous_jones
$ docker run --name redis -itd redis:latest
$ docker run -it -e REDIS_HOST=172.17.0.2 paynow:latest /bin/bash		run container and set environment REDIS_HOST=172.17.0.2
$ docker run --name redis -p 6379:6379 -itd redis:latest
-----------------------------------------------------------------

docker rm 127a287ff507													kill running process

-----------------------------------------------------------------
$ docker inspect nauseous_jones											view container configuration
$ docker inspect redis | grep tcp										view container configuration filter by tcp
-----------------------------------------------------------------

-----------------------------------------------------------------
$ docker start nauseous_jones
$ docker stop nauseous_jones
-----------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
Redis:

--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
Postgres:
$ docker run --name postgresql -e POSTGRES_PASSWORD=P@ssw0rd -p 5432:5432 -d postgres           run postgres
$ docker run -it --rm --link postgresql:postgres postgres psql -h postgres -U postgres          connect postgres via psql

postgres=# create user psbadmin with password 'P@ssw0rd';create database paynow_dev owner psbadmin;create database paynow_log owner psbadmin;

or

postgres=# create user psbadmin with password 'P@ssw0rd';
postgres=# create database paynow_dev owner psbadmin;
postgres=# create database paynow_log owner psbadmin;
postgres=# \l
postgres=# \q
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
Paynow:
$ docker run --name paynow -it paynow:latest /bin/bash
root@fdf477447928:/usr/src/app# node server.js

$ docker run --name paynow -d paynow:latest
$ docker run --name paynow --entrypoint /bin/bash -t paynow:latest
$ docker run --name paynow --entrypoint /bin/bash -p 3000:8080 -t paynow:latest
--------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------
Docker Image
$ docker build -t paynow:latest .		                                    build docker file in current directory
$ docker images
$ docker rmi [REPOSITORY/IMAGE ID]
--------------------------------------------------------------------------------------------------------------------------


VM
$ docker-machine inspect
$ docker-machine ip

$ docker run --name server -it --net=host -p 5000:8080 server:latest /bin/bash

$ curl http://localhost:80
$ docker exec -it [id] /bin/bash

Open file:                                              cat [file]
See os version:                                         cat /etc/*-release
Debian:
Install text editor:                                    apt-get update
                                                        apt-get install vim
CentOS:

Clear screen:                                           clear
Copy file from docker host to docker container:         docker cp controllers/cashcard/create_v2.js paynow:/usr/src/app/controllers/cashcard/create_v2.js


Use docker in other command line (bash/shell/cmd prompt)
$ docker-machine ls
$ docker-machine ssh <docker-machine name>

$ docker inspect postgres | grep IPAddress


[Volume]
$ docker run -it -v $(pwd):/usr/src/app <image-name> /bin/bash


[log]
$ docker logs -f <container-id>
$ docker logs <docker-container> 2>&1 | grep "<keyword>"


[copy file (docker v.1.7.1)]
cat testktb.js | docker exec -i psbdd sh -c 'cat > /usr/src/app/lib/controllers/testktb.js'


[exec with command]
docker exec <cid> <ls -la src/>
