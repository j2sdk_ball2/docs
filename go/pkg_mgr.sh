cd <project-dir>/

# install dependencies locally
godep get

# view environment variable
env

# install dependencies globally
go get golang.org/x/sys/unix

# install dependencies globally
go get github.com/codegangsta/gin

# start app
gin -a 4000 --all